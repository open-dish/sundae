import os

# Path to the Synthea file
SYNTHEA_PATH = os.getenv('SYNTHEA_PATH', '')

# Basic command to run Synthea
RUN_SYNTHEA = 'java -jar ' + SYNTHEA_PATH

# CSV files that will be excluded from the generated data
EXCLUDED_FILES_GENERATED_DATA = [
    'careplans.csv',
    'claims.csv',
    'claims_transactions.csv',
    'devices.csv',
    'imaging_studies.csv',
    'immunizations.csv',
    'organizations.csv',
    'patient_expenses.csv',
    'payer_transitions.csv',
    'payers.csv',
    'providers.csv',
    'supplies.csv'
]

# Used Synthea datasets
DATASETS = [
    'allergies',
    'conditions',
    'encounters',
    'medications',
    'observations',
    'patients',
    'procedures'
]

# Transformed datasets
TRANSFORMED_DATASETS = [
     'diagnostic_reports',
     'allergies',
     'encounters',
     'medications',
     'patients',
     'procedures'
 ]

# Main data of a blood test
BLOOD_TEST_DATA = {
    '718-7': 'Hemoglobin [Mass/volume] in Blood',
    '777-3': 'Platelets [#/volume] in Blood by Automated count',
    '787-2': 'MCV [Entitic volume] by Automated count',
    '786-4': 'MCHC [Mass/volume] by Automated count',
    '785-6': 'MCH [Entitic mass] by Automated count',
    '6690-2': 'Leukocytes [#/volume] in Blood by Automated count',
    '789-8': 'Erythrocytes [#/volume] in Blood by Automated',
    '4544-3': 'Hematocrit [Volume Fraction] of Blood by Automated count',
    '32207-3': 'Platelet distribution width [Entitic volume] in Blood by Automated count',
    '32623-1': 'Platelet mean volume [Entitic volume] in Blood by Automated count',
    '21000-5': 'Erythrocyte distribution width [Entitic volume] by Automated count'
}

# Fields used from each Synthea dataset
USED_FIELDS = dict()

USED_FIELDS['patients'] = [
    'Id',
    'BIRTHDATE',
    'DEATHDATE',
    'FIRST',
    'LAST',
    'MARITAL',
    'RACE',
    'ETHNICITY',
    'GENDER',
    'BIRTHPLACE',
    'LAT',
    'LON'
]

USED_FIELDS['observations'] = [
    'DATE',
    'PATIENT',
    'ENCOUNTER',
    'CATEGORY',
    'CODE',
    'DESCRIPTION',
    'VALUE',
    'UNITS',
    'TYPE'
]

USED_FIELDS['encounters'] = [
    'Id',
    'START',
    'STOP',
    'PATIENT',
    'PROVIDER',
    'ENCOUNTERCLASS',
    'CODE',
    'DESCRIPTION',
    'REASONCODE',
    'REASONDESCRIPTION'
]

# Complete Synthea dataset
USED_FIELDS['conditions'] = [
    'START',
    'STOP',
    'PATIENT',
    'ENCOUNTER',
    'CODE',
    'DESCRIPTION'
]

USED_FIELDS['medications'] = [
    'START',
    'STOP',
    'PATIENT',
    'ENCOUNTER',
    'CODE',
    'DESCRIPTION',
    'DISPENSES',
    'REASONCODE',
    'REASONDESCRIPTION'
]

# Complete Synthea dataset
USED_FIELDS['allergies'] = [
    'START',
    'STOP',
    'PATIENT',
    'ENCOUNTER',
    'CODE',
    'SYSTEM',
    'DESCRIPTION',
    'TYPE',
    'CATEGORY',
    'REACTION1',
    'DESCRIPTION1',
    'SEVERITY1',
    'REACTION2',
    'DESCRIPTION2',
    'SEVERITY2'
]

USED_FIELDS['procedures'] = [
    'START',
    'STOP',
    'PATIENT',
    'ENCOUNTER',
    'CODE',
    'DESCRIPTION',
    'REASONCODE',
    'REASONDESCRIPTION'
]

# Routes for the model manager APIs
ML_MANAGER_APIS = {
    'ANEMIA_TEXT': ""
}

# Routes for the database APIs
DHIM_APIS = {
    'ALLERGY': '/allergy',
    'DIAGNOSTIC_REPORT': '/diagnostic',
    'ENCOUNTER': '/encounter',
    'MEDICATION': '/medication',
    'PATIENT': '/patient',
    'PROCEDURE': '/procedure'
}

# Routes for the database APIs - interactive version
INTERACTIVE_DHIM_APIS = {
    'ALLERGYS': '/allergy',
    'DIAGNOSTIC_REPORTS': '/diagnostic',
    'ENCOUNTERS': '/encounter',
    'MEDICATIONS': '/medication',
    'PATIENT': '/patient',
    'PROCEDURES': '/procedure'
}