from fastapi import APIRouter, status, BackgroundTasks, HTTPException
from .models import Patient
from .services import NextPatientService
from image import Image, NextImageService

np_service = NextPatientService(20) # Next-patient service
ni_service = NextImageService() # Next-image service

router = APIRouter() # API routes

@router.get('/',
            status_code=status.HTTP_200_OK,
            description="Information About the endpoints.",
            summary="Endpoint description.")
def about():
    return "API to simulate the patient's entry into the system."

@router.get('/next-patient',
            response_model=Patient,
            status_code=status.HTTP_200_OK,
            description="Endpoint used to get the id for the next patient.",
            summary="Get the next patient id.")
async def get_next_patient(background_tasks:BackgroundTasks):
    return {"id": np_service.get_patient(background_tasks)}

@router.get('/next-image',
            response_model=Image,
            status_code=status.HTTP_200_OK,
            description="Endpoint used to get data for inference of models that work with images.",
            summary="Get the next image data.")
async def get_next_image(background_tasks:BackgroundTasks):
    image = ni_service.get_image()
    if image == None:
        raise HTTPException(status_code = status.HTTP_404_NOT_FOUND,
                           detail="No image was found for inference at the moment.")
    background_tasks.add_task(ni_service.send_image_to_ML, image)
    return image