from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from .routes import router as npapi_router

np_api = FastAPI()

origins = [
    "*",
]

np_api.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

np_api.include_router(npapi_router, prefix="",
                     tags=["sundae-api"])