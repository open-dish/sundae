import os
import httpx
from delivery import DataSender
from fastapi import BackgroundTasks
from generation import generate_patients, JsonDataGenerator
from db import delete_patient_data, get_patient_data, store_patient_data

class NextPatientService():
    def __init__(self, patients_buffer_size: int) -> None:
        self.patients_buffer_size = 20 if patients_buffer_size < 20 else patients_buffer_size
        self.patients_min_buffer_size = int(patients_buffer_size/2)
        self.patients = []
        self.dhim_host = os.getenv('DHIM_HOST', 'dhim.api.open-dish.lab.ic.unicamp.br')
        self.dhim_port = os.getenv('DHIM_PORT', '80')
        self.generate_store_patient(patients_buffer_size)
    
    def __get_patient_data(self, patient_id:str, dfs:dict) -> dict:
        """Returns the data dictionary for one patient"""
        tables = ['allergies','diagnostic_reports','encounters','medications','procedures']
        patient_data = {}

        patient_data['patient'] = [json for json in dfs['patients'] if json['id'] == patient_id]

        for table in tables:
            patient_data[table] = [json for json in dfs[table] if json['patient'] == patient_id]

        return patient_data
    
    def get_patient(self, background_tasks:BackgroundTasks) -> str:
        '''
        Returns a patient ID, replenishes the buffer if necessary and posts the new patient.
        '''
        if len(self.patients) == self.patients_min_buffer_size:

            background_tasks.add_task(self.generate_store_patient,self.patients_min_buffer_size)

        return(self.retrieve_patient(background_tasks))

    def generate_store_patient(self, number_of_patients:int) -> None:
        '''
        Generates the patient registers and stores its json files (so it can be used later)
        '''
        json_data_generator = JsonDataGenerator(generate_patients(number_of_patients, all_alive=True))
        dfs = json_data_generator.get_json_data()

        patient_ids = json_data_generator.get_patients_list()
        self.patients += patient_ids        

        for patient in patient_ids:
            patient_data = self.__get_patient_data(patient, dfs)
            store_patient_data(patient, patient_data)

    def post_patient(self, patient_id:str, background_tasks:BackgroundTasks) -> bool:
        '''
        Posts the patient data to the DB and to the ML Manager API
        '''
        dfs = get_patient_data(patient_id)
        patient = dfs['patient'][0]

        data_sender = DataSender()
        dhim_client = httpx.Client(base_url=f'http://{self.dhim_host}:{self.dhim_port}')
        ml_manager_client = httpx.Client(base_url='http://localhost:8000')

        if not data_sender.send_patient(patient, dhim_client):
            return False            

        for allergies in dfs['allergies']:
            if not data_sender.send_allergy(allergies, dhim_client):
                return False

        for diagnostic_report in dfs['diagnostic_reports']:
            if not data_sender.send_diagnostic_report(diagnostic_report, dhim_client):
                return False
            if not data_sender.send_anemia_data(diagnostic_report, patient, ml_manager_client):
                return False

        for encounter in dfs['encounters']:
            if not data_sender.send_encounter(encounter, dhim_client):
                return False

        for medication in dfs['medications']:
            if not data_sender.send_medication(medication, dhim_client):
               return False

        for procedure in dfs['procedures']:
            if not data_sender.send_procedure(procedure, dhim_client):
                return False

        background_tasks.add_task(self.__remove_patient, patient_id)

        dhim_client.close()
        ml_manager_client.close()
        return True
    
    def __remove_patient(self, patient_id:str) -> None:
        '''
        Removes the patient from the local data
        '''
        delete_patient_data(patient_id)

    def retrieve_patient(self, background_tasks:BackgroundTasks) -> str:
        '''
        Picks a patient and retrieves it from the csv
        '''
        if len(self.patients) == 0:
            return ""

        patient_id = self.patients[0]

        if(patient_id == None):
            return ""

        if not self.post_patient(patient_id, background_tasks):
            return "" 

        return self.patients.pop(0)
