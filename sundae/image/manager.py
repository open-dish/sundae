import os
import json
import pandas as pd
from .image_model import Image

class ImageDBManager:

    database_csv: str
    database_json: dict
    database: list[Image]
    database_size: int

    def __init__(self) -> None:
        self.database_csv = os.getenv('SUNDAE_IMAGEDB_CSV', 'imagedb.csv')
        self.__open_database_csv()
        self.database = list()
        self.__create_database()
        self.database_size = len(self.database)

    def __open_database_csv(self) -> None:
        """Open the csv file and transform it in a list of json(dicts)."""
        df_json = pd.read_csv(self.database_csv).to_json(orient='records')
        self.database_json = json.loads(df_json)

    def __create_database(self) -> None:
        """Create a list of objects containing the image information."""

        self.database = [
            Image(**image_json)
            for image_json in self.database_json
        ]

    def get_image_obj(self) -> dict[str, dict[str, str]]:
        """Return an Image Object from database."""
        try:
            self.database_size -= 1
            return self.database.pop().dict()
        except IndexError as err:
            self.database_size = len(self.database)
            # TODO: Log manager
            print(f"[SUNDAE->IMAGEDB_MANAGER]: Unable to return an image from the database!")
            print(f"ERROR: {err}")
            return None # type: ignore

    def get_images(self, size:int=10) -> list[dict[str, dict[str, str]]]:
        """Return a set of image Objects from database."""
        if self.database_size == 0:
            print("[SUNDAE->IMAGEDB_MANAGER]: The image database is empty. Unable to upload any image.")
            return None
        
        if size <= 0 or size > self.database_size:
            print("[SUNDAE->IMAGEDB_MANAGER]: Invalid number was passed, using default value (10).")
            size = 10

        return [self.get_image_obj() for _ in range(size)]