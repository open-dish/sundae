from pydantic import BaseModel, Field
from datetime import datetime

class Image(BaseModel):
    id: str
    patient: str
    encounter: str
    imagePath: str
    annotationRaw: str
    date: str = Field(
                       default_factory = 
                       lambda: datetime.utcnow().strftime("%Y-%m-%dT%H:%M:%SZ")
                     )