import os
from pymongo import MongoClient
from .manager import ImageDBManager

def populate_imagedb():
    imagedb_manager = ImageDBManager()

    images = imagedb_manager.get_images(imagedb_manager.database_size)
    mongo_host = os.getenv('MONGO_HOST', 'mongodb://open-dish:secret@open-dish.lab.ic.unicamp.br:27017')
    database = os.getenv('MONGODB_NAME', 'sundae_test')
    client = MongoClient(mongo_host)
    db = client[database]
    image_collection = db['image']

    image_collection.insert_many(images)