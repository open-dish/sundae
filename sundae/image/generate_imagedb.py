from pathlib import Path
import os
import re
from binascii import b2a_hex
import csv

def replace_string(string:str) -> str:
    r_string = re.sub(r'Images/', r'Annotations/', string)
    return re.sub(r'jpg', r'xml', r_string)

def gen_id(size:int) -> str:
    """Generate the ids for images, patients and encounters"""
    raw = b2a_hex(os.urandom(size)).decode('utf-8')
    id = f"{raw[:8]}-{raw[8:12]}-{raw[12:16]}-{raw[16:20]}-{raw[20:]}"
    return id

def gen_img_std_name(img:Path, img_annot:Path, img_id:str) -> str:
    """Standardize image name from its id"""
    img_path = list(img.parts)
    img_path[-1] = f"{img_id}.{img_path[-1].split('.')[-1]}"
    std_name = '/'.join(img_path)[1:]
    img.rename(std_name)

    annot_path = list(img_annot.parts)
    annot_path[-1] = f"{img_id}.{annot_path[-1].split('.')[-1]}"
    annot_std_name = '/'.join(annot_path)[1:]
    img_annot.rename(annot_std_name)

    return (img_id, gen_id(16), gen_id(16), std_name, annot_std_name)

sundae_local_imagedb = os.getenv('SUNDAE_LOCAL_IMGDB', '')

database_paths = [Path(str(file.absolute()))
                  for file in Path(sundae_local_imagedb).rglob('*.jpg')]

annotation_paths = [Path(replace_string(str(file_path.absolute())))
                    for file_path in database_paths]


image_ids = [gen_id(16) for _ in range(len(database_paths))]

database = [
            gen_img_std_name(img, img_annot, img_id)
            for img, img_annot, img_id in zip(database_paths,
                                              annotation_paths, 
                                              image_ids)
           ]

header = ['id', "patient", "encounter", 'imagePath', 'annotationRaw']

imagedb_path = os.getenv('SUNDAE_IMAGEDB_CSV', 'imagedb.csv')

with open(imagedb_path, 'w', newline='') as f:
    writer = csv.writer(f)
    writer.writerow(header)
    writer.writerows(database)