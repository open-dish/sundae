import os
from .sender import ImageSender
from .image_model import Image
from .manager import ImageDBManager

class NextImageService:
    server_url: str
    sender: ImageSender
    imgdb_manager: ImageDBManager

    def __init__(self) -> None:
        self.server_url = os.getenv('ML_MANAGER_HOST', 'http://ml-manager:8080') 
        self.sender = ImageSender(f'{self.server_url}/images')
        self.imgdb_manager = self.sender.imagedb_manager
    
    def get_image(self) -> dict[str, str]:
        image = self.imgdb_manager.get_image_obj()
        if image:
            return image
        return {"error": "No images for inference at the moment."}
    
    def send_image_to_ML(self, image: dict[str, str]):
        self.sender.send_img_to_ml(image)