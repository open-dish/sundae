import asyncio
import httpx
from .manager import ImageDBManager

class ImageSender:

    server_url: str
    imagedb_manager: ImageDBManager

    def __init__(self, server_url) -> None:
        self.server_url = server_url
        self.imagedb_manager = ImageDBManager()

    def send_img(self) -> bool:
        """Send one image to server database."""
        img_obj = self.imagedb_manager.get_image_obj()
        if not img_obj:
            return False

        resp = httpx.post(self.server_url, data=img_obj)
        if resp.status_code == 201:
            return True
        
        return False

    def send_img_to_ml(self, image:dict[str, str]):
        # TODO: Enable sending images to ML Manager
        # response = httpx.post(self.server_url, data=image)

        # if response.status_code == 201:
        #     return True
        
        # return False
        return True

    async def send_img_batch(self, batch_size=10) -> int:
        """
        Send a batch of images to the server database
        Return: Number of images sent
        """
        img_batch = self.imagedb_manager.get_images(batch_size)
        if not img_batch:
            return 0
        
        failed_requests = list()
        async with httpx.AsyncClient() as client:

            requests = [
                client.post(self.server_url, data=img)
                for img in img_batch
            ]

            responses = await asyncio.gather(*requests)
            failed_requests = filter(lambda response: response.status_code != 201,
                                      responses)

        return batch_size - len(failed_requests)