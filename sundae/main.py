from fastapi_utils.tasks import repeat_every
from .api.v1 import np_api
from .delivery import ConstantGenerator

constant_generation_server = ConstantGenerator(20)
countdown_time = 60 * 60 * 24 # 1 day

# @np_api.on_event("startup")
@repeat_every(seconds=countdown_time)  # 1 day
def daily_generation() -> None:
    constant_generation_server.populate()