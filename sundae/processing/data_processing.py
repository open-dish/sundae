import pandas as pd

import utils.constants as CONST


class DataProcessing:

    def __init__(self, files_path:str) -> None:
        self.raw_data = dict()
        self.interim_data = dict()
        self.processed_data = dict()
        self.complete_blood_test_patients = list()
        self.complete_blood_test_encounters = list()
        self.files_path = files_path
        
        
    def __get_used_fields(self, data:dict, used_fields:dict) -> dict:
        """
        Drop unused columns from datasets.
        Returns clean datasets in dict. 
        """
        for dataset in data.keys():
            data[dataset] = data[dataset][used_fields[dataset[3:]]]
        return data
    
    
    def __read_raw_data(self, datasets:list=None) -> dict:  # type: ignore
        """
        Read all generated data.
        Returns datasets in dict.
        """
        datasets = datasets or CONST.DATASETS
        try: 
            for dataset in datasets:
                df = f'df_{dataset}'
                self.raw_data[df] = pd.read_csv(f'{self.files_path}/{dataset}.csv')
        except FileNotFoundError as e:
            print(f'File not found in directory {self.files_path}.')
            print(f'Error: {e}')

        return self.raw_data
    
    def __get_complete_blood_test_encounters(self) -> list:
        """
        Get all encounters with complete blood test from the datasets.
        Returns a list of all encounters with a complete blood test.
        """
        self.interim_data = self.__get_used_fields(self.get_raw_data(), CONST.USED_FIELDS)
        observations_encounters = self.interim_data['df_observations'].groupby('ENCOUNTER').CODE.unique()
        encounter_code_list = pd.DataFrame({
            'ENCOUNTER': observations_encounters.index,
            'CODE_LIST': observations_encounters.values
        })

        is_complete_blood_test = set(CONST.BLOOD_TEST_DATA.keys()).issubset
        self.complete_blood_test_encounters = encounter_code_list[
            [is_complete_blood_test(l) for l in encounter_code_list.CODE_LIST.values.tolist()]  # type: ignore
        ]['ENCOUNTER'].tolist()
        return self.complete_blood_test_encounters

    
    def __get_complete_blood_test_patients(self) -> list:
        """
        Get all patients with complete blood test from the datasets.
        Returns a list of all patients with a complete blood test.
        """
        complete_blood_test_encounters = self.get_complete_blood_test_encounters()

        self.complete_blood_test_patients = self.interim_data['df_observations'].query(
            'ENCOUNTER in @complete_blood_test_encounters')['PATIENT'].drop_duplicates().reset_index(drop=True).tolist()
        
        return self.complete_blood_test_patients


    def __get_useful_data(self) -> dict:
        """
        Drop all data of patients who do not have complete blood test.
        Returns useful data to datasets in dict. 
        """
        complete_blood_test_patients = self.get_complete_blood_test_patients()
        for dataset in CONST.DATASETS:
            if(dataset == 'patients'):
                self.processed_data[f'df_{dataset}'] = self.interim_data[f'df_{dataset}'].query(f'Id in {complete_blood_test_patients}')
            else:
                self.processed_data[f'df_{dataset}'] = self.interim_data[f'df_{dataset}'].query(f'PATIENT in {complete_blood_test_patients}')
            self.processed_data[f'df_{dataset}'] = self.processed_data[f'df_{dataset}'].astype(str)
        return self.processed_data
    
    
    def get_raw_data(self) -> dict:
        """
        Returns raw data in dict.
        """
        if self.raw_data:
            return self.raw_data
        return self.__read_raw_data()
    
    def get_complete_blood_test_encounters(self) -> list:
        """
        Returns a list of all encounters with a complete blood test.
        """
        if self.complete_blood_test_encounters:
            return self.complete_blood_test_encounters
        return self.__get_complete_blood_test_encounters()
    
    
    def get_complete_blood_test_patients(self) -> list:
        """
        Returns a list of all patients with a complete blood test.
        """
        if self.complete_blood_test_patients:
            return self.complete_blood_test_patients
        return self.__get_complete_blood_test_patients()
    
    
    def get_processed_data(self) -> dict:
        """
        Returns processed/useful data in dict.
        """
        if self.processed_data:
            return self.processed_data
        return self.__get_useful_data()
