from binascii import b2a_hex
import pandas as pd
import os

class DataTransformation:
    def __init__(self, dfs: dict) -> None:
        self.dfs = dfs

    def __gen_id(self, size:int) -> str:
        """Generate the ids for images, patients and encounters"""
        raw = b2a_hex(os.urandom(size)).decode('utf-8')
        id = f"{raw[:8]}-{raw[8:12]}-{raw[12:16]}-{raw[16:20]}-{raw[20:]}"
        return id

    def __transform_allergies(self) -> pd.DataFrame:
        """
        Transforms allergies csv to server database API standards.
        Returns transformed csv.
        """
        #there are no uppercase letters
        df = self.dfs['df_allergies']
        df.columns = df.columns.str.lower()
        return df

    def __transform_encounters(self) -> pd.DataFrame:
        """
        Transforms encounters csv to server database API standards.
        Returns transformed csv.
        """
        df = self.dfs['df_encounters']
        df.columns = df.columns.str.lower()
        df = df.rename(columns={"encounterclass": "encounterClass", "reasoncode": "reasonCode", "reasondescription":"reasonDescription", "provider":"participantActor"})
        return df

    def __transform_medications(self) -> pd.DataFrame:
        """
        Transforms medications csv to server database API standards.
        Returns transformed csv.
        """
        df = self.dfs['df_medications']
        df.columns = df.columns.str.lower()
        df = df.rename(columns={"reasoncode": "reasonCode", "reasondescription": "reasonDescription"})
        return df

    def __transform_patients(self) -> pd.DataFrame:
        """
        Transforms patients csv to server database API standards.
        Returns transformed csv.
        """
        df = self.dfs['df_patients']
        df.columns = df.columns.str.lower()
        df = df.drop("marital", axis=1)
        df = df.rename(columns={"birthdate":"birthDate","deathdate":"deathDate","birthplace":"birthPlace"})
        return df

    def __transform_procedures(self) -> pd.DataFrame:
        """
        Transforms procedures csv to server database API standards.
        Returns transformed csv.
        """
        df = self.dfs['df_procedures']
        df.columns= df.columns.str.lower()
        df = df.drop(["description","reasondescription"], axis=1)
        df = df.rename(columns={"reasoncode":"reasonCode"})
        return df

    def __transform_diagnostic_reports(self) -> pd.DataFrame:
        """
        Creates diagnostic reports csv in server database API standards.
        Returns transformed csv.
        """
        df_encounters = self.dfs['df_encounters']
        df_conditions = self.dfs['df_conditions']
        df_observations = self.dfs['df_observations']

        # Drop and rename some columns
        df_encounters = df_encounters.loc[:, ['Id', 'PATIENT', 'PROVIDER']]
        df_encounters.columns = ['encounter', 'patient', 'resultsInterpreter']

        df_conditions = df_conditions.loc[:, ['PATIENT', 'ENCOUNTER', 'CODE']]
        df_conditions.columns = ['patient', 'encounter', 'conclusionCode']

        df_observations = df_observations.loc[:, ['PATIENT', 'ENCOUNTER', 'CATEGORY', 'CODE', 'VALUE', 'UNITS', 'TYPE']]
        df_observations = df_observations.rename(columns = {'PATIENT': 'patient', 'ENCOUNTER': 'encounter'})
        
        def observation_to_obj(category, code, value, units, type):
            """
            Build the observation object.
            """
            return {
                'category': category,
                'code': code,
                'value': value,
                'units': units,
                'type': type
            }
            
        # Build the encounter objects 
        df_observations['encounter_obj'] = df_observations\
            .apply(lambda row: observation_to_obj(row['CATEGORY'], row['CODE'], row['VALUE'], row['UNITS'], row['TYPE']), axis=1)
        df_observations.drop(columns=['CATEGORY', 'CODE', 'VALUE', 'UNITS', 'TYPE'], inplace=True)
        
        # Cast the conclusionCodes to int
        df_conditions = df_conditions.astype({'conclusionCode': 'int64'})

        # Build the conclusionCode and result lists
        df_conditions = df_conditions.groupby(['patient', 'encounter'])['conclusionCode']\
            .apply(list).reset_index(name='conclusionCode')
        df_observations = df_observations.groupby(['patient', 'encounter'])['encounter_obj']\
            .apply(list).reset_index(name='result')

        # Merge the dataframes in the df_diagnostic_reports dataframe
        df_diagnostic_reports = pd.merge(df_observations, df_conditions, how='outer', on=['patient', 'encounter'])
        df_diagnostic_reports = pd.merge(df_diagnostic_reports, df_encounters, how='right', on=['patient', 'encounter'])
        
        # Add some new data to df_diagnostic_reports
        df_diagnostic_reports['status'] = 'final'
        df_diagnostic_reports['id'] = df_diagnostic_reports.apply(lambda y: self.__gen_id(16), axis=1)
        df_diagnostic_reports['study'] = [[''] for _ in range(df_diagnostic_reports.shape[0])]
        df_diagnostic_reports['reliability'] = [[1] for _ in range(df_diagnostic_reports.shape[0])]
        
        # Add result into an 'observation' object 
        df_diagnostic_reports['result'] = df_diagnostic_reports['result']\
            .apply(lambda y: {'observation': y} if type(y)==list  else {'observation': []})

        # Add [0] to empty lists (NaN) in conclusionCode
        df_diagnostic_reports['conclusionCode'] = df_diagnostic_reports['conclusionCode']\
            .apply(lambda y: [0] if isinstance(y, float) else y)

        return df_diagnostic_reports

    def transform_all_data(self) -> dict:
        """
        Transforms all data to server database API standards.
        Returns transformed dataframes in dict.
        """
        self.dfs['df_diagnostic_reports'] = self.__transform_diagnostic_reports() #assume que as modificações nos outros dfs não foram feitas
        self.dfs['df_allergies'] = self.__transform_allergies()
        self.dfs['df_encounters'] = self.__transform_encounters()
        self.dfs['df_medications'] = self.__transform_medications()
        self.dfs['df_patients'] = self.__transform_patients()
        self.dfs['df_procedures'] = self.__transform_procedures()

        return self.dfs