import os
import asyncio
from httpx import Client, AsyncClient
import utils.constants as CONST

class DataSender:
    def __init__(self) -> None:
        self.dhim_host = os.getenv('DHIM_HOST', 'dhim.api.open-dish.lab.ic.unicamp.br')
        self.dhim_port = os.getenv('DHIM_PORT', '80')

    async def send_complete_data(self, data:dict) -> bool:
        """Sends all patient data in batch and asynchronous."""
        async with AsyncClient(base_url=f'http://{self.dhim_host}:{self.dhim_port}') as client:
            
            patient = data.pop('patient')
            patient_task = client.post(CONST.INTERACTIVE_DHIM_APIS['PATIENT'], json=patient[0], timeout=None)

            tasks = [
                client.post(CONST.INTERACTIVE_DHIM_APIS[key.upper()], json=item, timeout=None)
                for key, value in data.items() for item in value 
            ]

            results = await asyncio.gather(patient_task, *tasks)

            failure = list(filter(lambda resp: resp.status_code != 200, results))
            
            if len(failure):
                print("An error occurred with the following queries: {}".format(*failure))
                return False
            
            return True

    def send_allergy(self, allergy: dict, client:Client) -> bool:
        """Sends one allergies file to server database."""
        resp = client.post(CONST.DHIM_APIS['ALLERGY'], json=allergy, timeout=None)

        if resp.status_code == 201:
            return True
        return False
    
    def send_anemia_data(self, diagnostic_report:dict, patient:dict, client:Client) -> bool:
        """Sends one json with the required data to the ML model manager to get ML inferred results for anemia"""
        complete_json = {"patient": patient, "diagnostic_report": diagnostic_report}
        #resp = client.post(CONST.ML_MANAGER_APIS['ANEMIA_TEXT'], json=complete_json)

        return True
        return False

    def send_diagnostic_report(self, diagnostic_report: dict, client:Client) -> bool:
        """Sends one diagnostic report to server database."""
        resp = client.post(CONST.DHIM_APIS['DIAGNOSTIC_REPORT'], json=diagnostic_report, timeout=None)

        if resp.status_code == 201:
            return True
        return False

    def send_encounter(self, encounter: dict, client:Client) -> bool:
        """Sends one encounter to server database."""
        resp = client.post(CONST.DHIM_APIS['ENCOUNTER'], json=encounter, timeout=None)

        if resp.status_code == 201:
            return True
        return False

    def send_medication(self, medication: dict, client:Client) -> bool:
        """Sends one medications file to server database."""
        resp = client.post(CONST.DHIM_APIS['MEDICATION'], json=medication, timeout=None)

        if resp.status_code == 201:
            return True
        return False

    def send_patient(self, patient: dict, client:Client) -> bool:
        """Sends one patient to server database."""
        resp = client.post(CONST.DHIM_APIS['PATIENT'], json=patient, timeout=None)

        if resp.status_code == 201:
            return True
        return False

    def send_procedure(self, procedure: dict, client:Client) -> bool:
        """Sends one procedures file to server database."""
        resp = client.post(CONST.DHIM_APIS['PROCEDURE'], json=procedure, timeout=None)

        if resp.status_code == 201:
            return True
        return False
