import os
import httpx
from . import DataSender
from generation import generate_patients, JsonDataGenerator

class ConstantGenerator:
    def __init__(self, number_of_patients:int) -> None:
        self.number_of_patients = number_of_patients
        self.dhim_host = os.getenv('DHIM_HOST', 'dhim.api.open-dish.lab.ic.unicamp.br')
        self.dhim_port = os.getenv('DHIM_PORT', '80')

    def populate(self) -> bool:
        """Generates the patients' data and posts it to the database"""
        json_data_generator = JsonDataGenerator(generate_patients(self.number_of_patients, all_alive=True))
        dfs = json_data_generator.get_json_data()

        return self.__send_data(dfs)

    def __send_data(self, dfs:dict) -> bool:
        """Posts the data to the database"""
        data_sender = DataSender()
        dhim_client = httpx.Client(base_url=f'http://{self.dhim_host}:{self.dhim_port}')

        for allergies in dfs['allergies']:
            if not data_sender.send_allergy(allergies, dhim_client):
                return False

        for diagnostic_report in dfs['diagnostic_reports']:
            if not data_sender.send_diagnostic_report(diagnostic_report, dhim_client):
                return False

        for encounter in dfs['encounters']:
            if not data_sender.send_encounter(encounter, dhim_client):
                return False

        for medication in dfs['medications']:
            if not data_sender.send_medication(medication, dhim_client):
               return False

        for patient in dfs['patients']:
            if not data_sender.send_patient(patient, dhim_client):
                return False

        for procedure in dfs['procedures']:
            if not data_sender.send_procedure(procedure, dhim_client):
                return False

        dhim_client.close()
        return True