import pandas as pd
import json
from processing import DataProcessing, DataTransformation
import utils.constants as CONST

class JsonDataGenerator:

    def __init__(self, files_path:str) -> None:
        self.processing_data = DataProcessing(files_path)
        self.files_path = files_path
        self.json_data = dict()
        self.final_data_ml_manager = dict()
        
        
    def __csv_to_json_list(self, df:pd.DataFrame) -> list:
        """Converts a csv file to a list of json"""
        df_json = df.to_json(orient='records')
        return json.loads(df_json)

    
    def get_patients_list(self) -> list:
        """Return the useful patients"""
        return self.processing_data.get_complete_blood_test_patients()


    def get_json_data(self) -> dict:
        """
        Get all transformed data to json
        The num_patients is not necessarily the same as the number of json returned.
        """
        if self.json_data:
            return self.json_data

        data_transformation = DataTransformation(self.processing_data.get_processed_data()).transform_all_data()
        
        for dataset in CONST.TRANSFORMED_DATASETS:
            self.json_data[dataset] = self.__csv_to_json_list(data_transformation[f'df_{dataset}'])
        
        return self.json_data
    
    
    def get_complete_blood_test_patient_diagnostic_json_data(self) -> dict:
        """
        Get all complete blood test diagnostic reports in json
        """
        if self.final_data_ml_manager:
            return self.final_data_ml_manager

        data_transformation = DataTransformation(self.processing_data.get_processed_data()).transform_all_data()
        
        complete_blood_test_encounters = self.processing_data.get_complete_blood_test_encounters()
        
        complete_blood_test_diagnostic_reports = data_transformation['df_diagnostic_reports'].query('encounter in @complete_blood_test_encounters')
        
        complete_blood_test_patient_diagnostic = dict()
        
        for patient_id in complete_blood_test_diagnostic_reports['patient'].drop_duplicates():
            patient_data = self.__csv_to_json_list(data_transformation['df_patients'].query("id == @patient_id"))[0]
            
            complete_blood_test_patient_diagnostic[patient_id] = self.__csv_to_json_list(
                complete_blood_test_diagnostic_reports.loc[
                    complete_blood_test_diagnostic_reports['patient']==patient_id
                ]
            )
            
            ml_manager_sigle_data = [
                {"patient": patient_data, "diagnostic": diagnostic} for diagnostic in complete_blood_test_patient_diagnostic[patient_id]
            ]
            
            self.final_data_ml_manager[patient_id] = ml_manager_sigle_data
        
        return self.final_data_ml_manager
    