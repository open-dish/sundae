import utils.constants as CONST
import subprocess
from datetime import datetime

def generate_patients(
        num_patients: int, all_alive: bool = False,
        all_dead: bool = False):
    """Generate realistic synthetic electronic health records (RS-EHRs) 
    using the basic setup of Synthea.

    Arguments:
    num_patients (int)  -- the number of patients to be generated
    all_alive (bool) -- if every patient generated will be alive or not
    all_dead (bool) -- if every patient generated will be dead or not

    Return:
    base_directory (str) -- the path to the generated data folder
    """
    # String containing the CSV files not to be generated
    excluded_files = ','.join(CONST.EXCLUDED_FILES_GENERATED_DATA)

    # Convert to string
    all_alive_str = 'true' if all_alive else 'false'
    all_dead_str = 'true' if all_dead else 'false'

    # Get the actual date and time 
    date_time = datetime.now()
    date_time = date_time.strftime('D%Y_%m_%d-T%H_%M_%S')

    # Path to the generated data
    base_directory = f'./data/{date_time}/'

    # Define the shell command to generate the Synthea data
    command = CONST.RUN_SYNTHEA \
              + f' -p {num_patients}' \
              + ' --generate.append_numbers_to_person_names=false' \
              + f' --generate.only_alive_patients={all_alive_str}' \
              + f' --generate.only_dead_patients={all_dead_str}' \
              + f' --exporter.baseDirectory={base_directory}' \
              + ' --exporter.csv.export=true' \
              + f' --exporter.csv.excluded_files={excluded_files}' \
              + ' --exporter.metadata.export=false' \
              + ' --exporter.fhir.export=false' \
              + ' --exporter.fhir.transaction_bundle=false' \
              + ' --exporter.hospital.fhir.export=false' \
              + ' --exporter.practitioner.fhir.export=false'

    # Run Synthea
    subprocess.run(command, shell=True)

    # Complete the path
    base_directory += 'csv/'

    return base_directory
