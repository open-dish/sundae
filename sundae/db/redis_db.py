import json
import os
import redis
import ast

patient_stock = redis.Redis(
    host = os.getenv("REDIS_HOST", "open-dish.lab.ic.unicamp.br"),
    port = int(os.getenv("REDIS_PORT", 6379)),
    db = int(os.getenv("REDIS_DB", 0)),
    #password = os.getenv("REDIS_PWD", 'teste')
)

def delete_patient_data(patient_id:str) -> None:
    """Deletes the patient data from the database"""
    patient_stock.delete(patient_id)

def get_patient_data(patient_id:str) -> dict:
    """Retrieves the patient data from the database"""
    data = str(patient_stock.get(patient_id))
    data = data[2:-1]
    data = data.replace("\"", "'")
    data = data.replace("\\\'", "\"")
    
    return ast.literal_eval(data)

def store_patient_data(patient_id:str, patient_data:dict) -> None:
    """Stores the patient data in the database"""
    patient_stock.set(patient_id, str(patient_data))