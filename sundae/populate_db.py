import os
import pymongo
from generation import generate_patients, JsonDataGenerator
from image import ImageDBManager

# Get Environment Variables
mongo_host = os.getenv('MONGO_HOST', 'mongodb://open-dish:secret@open-dish.lab.ic.unicamp.br:27017')
database = os.getenv('MONGODB_NAME', 'populate_db_test')

# Define the client and the database
client = pymongo.MongoClient(mongo_host)
database = client[database]

# Generate the synthetic data
NUM_PATIENTS = 10_000
json_data_generator = JsonDataGenerator(generate_patients(NUM_PATIENTS))
data = json_data_generator.get_json_data()

# Iterate through the collections
for key in data.keys():
    collection = database[key]
    collection.insert_many(data[key])

imagedb_manager = ImageDBManager()
images = imagedb_manager.get_images(imagedb_manager.database_size)
image_collection = database['image']
image_collection.insert_many(images)