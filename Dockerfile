FROM python:3.9-slim-buster

ENV JAVA_HOME=/opt/java/openjdk
COPY --from=eclipse-temurin:17-jre $JAVA_HOME $JAVA_HOME
ENV PATH="${JAVA_HOME}/bin:${PATH}" SYNTHEA_PATH="/opt/synthea/synthea-with-dependencies.jar"
ENV SUNDAE_LOCAL_IMAGEDB="/image_database/blood-cells/" SUNDAE_IMAGEDB_CSV="/image_database/imagedb.csv"

ARG DEBIAN_FRONTEND=noninteractive
RUN apt update && apt upgrade -y && apt install -y wget unzip && mkdir -p /opt/synthea && \
    wget -P /opt/synthea/\
    https://github.com/synthetichealth/synthea/releases/download/master-branch-latest/synthea-with-dependencies.jar


# Enable to construct the image database
# COPY ./sundae/image/generate_imagedb.py /
# RUN mkdir -p /image_database && cd /image_database && \
#     wget https://github.com/MahmudulAlam/Complete-Blood-Cell-Count-Dataset/archive/master.zip && \
#     unzip master.zip && mv Complete-Blood-Cell-Count-Dataset-master blood-cells && cd blood-cells && \
#     rm _config.yml LICENSE README.md && python /generate_imagedb.py

WORKDIR /code

COPY ./requirements/common.txt /code

RUN pip install -r common.txt
 
COPY . /code

EXPOSE 5483

CMD ["uvicorn", "sundae.main:np_api", "--host", "0.0.0.0", "--port", "5483"]