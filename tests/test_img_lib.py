from src.imglib.manager import ImageDBManager
from src.imglib.sender import ImageSender


def test_ImageDBManager_creation():
    url = 'https://drive.google.com/uc?export=download&id=1t7zReILeQC-oB1NQ5t4k9o6urYGxWjaG'
    manager = ImageDBManager(url)
    assert (manager.local_imgdb != None and manager.imgdb != None)

def test_ImageSender_creation():
    server_url = 'localhost:3000/images'
    sender = ImageSender(server_url)
    assert (sender.imgdbManager != None and sender.server_url == server_url)