# Data & Interface Modeling and Synthetic Data Generation

## Description

The Open DISH focuses on providing integration and interoperability between data science services and hospital infrastructure. In this context, SUnDaE, Synthetic Unit of Data Emulation, is a data provider for the main project, generating synthetic hospital data to feed the system. SUnDaE is based on Synthea and provides services such as Constant Patient Generator, Next Patient API and Image Service, in addition to carrying out the initial database population. All data for all systems are born here and are sent to Data Manager and Machine Learning Manager modules.

# Team Datassauros

* Artur Abreu Hendler
  * Table transformation
  * Next Patient Service
  * Constant Patient Generator
* Cristiano Sampaio Pinheiro
  * Data modeling
  * Data processing
  * Data Generator module
  * Optimization of transformations
  * Improve API performance
* George Gigilas Junior
  * Data modeling
  * Table transformation
  * Next Patient Service
  * Constant Patient Generator
  * Integration with other groups’ API
  * Issue distribution
* Jhonatan Cléto
  * Data modeling
  * Table transformation
  * Next Patient Service
  * Constant Patient Generator
  * Integration with other groups’ API
  * Issue distribution
* Mylena Roberta dos Santos
  * Synthea “specialist”
  * Data modeling
  * Raw data generator
  * Data file structuring
  * Data processing
  * Optimization of transformations
  * Database population

# GitLab Subproject

* [Synthetic Unit of Data Emulation (SUnDaE)](https://gitlab.com/open-dish/sundae)

# Folder and Files Structure

~~~
├── requirements  	        <- list of project dependencies
├── sundae
│   ├── api		        <- REST api service (using FastAPI)
│   │   └── v1
│   ├── db			<- intermediate database module (using Redis)
│   ├── delivery		<- external communication module
│   ├── generation		<- Synthea data generation module
│   ├── image			<- images modeling module
│   ├── processing		<- processing data module
│   ├── utils			<- some universal tools of the project
│   ├── main.py			<- initialize the SUnDaE system
|	└── populate_db.py	<- script to initial database population
├── tests	                <- unit tests
└──  docker-compose.yml	        <- Docker application configuration
~~~

# General Architecture or Process

![System diagram](images/system_diagram.png)  
_Figure 1: SUnDaE architecture overview._

For the implementation of services provided by SUnDaE, we use a modular architecture separating each of the different services offered in a dedicated module. Each module was implemented in a different `python` package. Figure 1 presents a schematic overview of all services provided by SUnDaE. Service descriptions are presented in the sections below.

## Generator Module

![Data Path Diagram](images/data_path_diagram.png)  
_Figure 2: Generator module data flow diagram._

The first step on the flow is the generation of the data itself, which includes patient information, observations, medications and other relevant data described later. To achieve this, the Synthea executable is used through a `python` script, setting some parameters such as number of patients and if some of them are alive or not. 

The next step then converts its format and processes it to drop some irrelevant data to this system usage scenario, forwarding the output to the last step. It is important to mention that during data processing, the patients selected have at least one complete blood test in their history, ensuring that there will be data for the other parts of the system to work.

In the table transformation module, some preparations regarding the database API model need to be done, the tables received here are combined and some of its fields are renamed to achieve this. These table entries are then converted to JSON documents to be sent via POST requests to the database, either through the next patient module or the constant generator one.

## Constant Patient Generator

While our job is to generate synthetic hospital data to emulate a real hospital system, we also need a way to emulate new patients coming to the hospital from time to time. For this reason, we created the Constant Patient Generator, which generates new patients whenever you call its `populate()` method. In order for this method to be repeatedly called from time to time, we used FastAPI’s repeated task and programmed our method to be called once a day (which could be changed depending on what’s needed).

![Constant generator diagram](images/constant_generator_diagram.png)  
_Figure 3: Constant Patient Generator architecture overview._

## Patient Stock (Next Patient Service)

Because of how long it can take to generate a new patient, we created a patient stock, so that we already have some patients ready. This way, when the front end API requests the next patient (through Next Patient API), we can send it right away. For this to work, we created a Redis database, which stores data in the `<key, value>` format, just like in `python` dictionaries. We chose Redis because its data resides in memory, which improves performance, it’s easy to use and has high scalability. 

In the current production environment, the data key is a patient id and its value is a dictionary of lists. With this approach, we can retrieve a patient id quickly and get all its data to be sent to the patient resource manager database (from where the front end will request the patient’s data). We also have a buffer of patient ids that, whenever it gets low on patients, can generate more patients in the background. This way it’s highly unlikely it will ever run out of patients. However, because of the way the patient resource manager database APIs are implemented, it takes a lot of time to finish sending all the data to them. We can’t send the patient id while its data isn’t in the patient resource manager database, because it wouldn’t be able to find its data. Therefore, while the patient stock saves a lot of time regarding the data generator, it’s still a bit slow because of the API dependence.

![Next patient API diagram](images/next_patient_api_diagram.png)  
_Figure 4: Next Patient Service architecture overview._

## Image Service

![Image Service Diagram](images/image_service_diagram.png)  
_Figure 5: Image service diagram._

The image service is responsible for sending images to the Patient Resource Manager, Machine Learning Model Manager, and an external client from the next image API. To do so, it manages images in a pre-built repository using a *docker volume* shared between all containers. The idea of building the image repository as a docker volume is to allow all consumers of the image service, connected to the Open DISH network, to have access to the images through the absolute path informed in the API response.

As specified in the planning stages, the images are identified using fictitious patient and encounter ids, since the images used in the database are unrelated to the patients generated by Synthea. In this sense, the image service acts as an image provider for a proof of concept of machine learning services that work with tasks involving images.

Commenting on the implementation of the service, we used the object-oriented programming paradigm and the principle of separation of concerns. We implement the ImageDBManager class responsible for managing the images and associated metadata, and the ImageSender class, responsible for delivering the images to clients asynchronously. Both classes are used by the NextImageService class, which regulates which and how many images will be sent and to which clients they will be sent.


# Installation and Execution

Before starting the SUnDaE service, you need an instance of the `Manager` component of Open DISH running in some host. `Manager` is the patient resource database manager. SUnDaE depends on it to provide its services. To install `Manager` you can follow [this instructions](https://gitlab.com/open-dish/dish/-/tree/development/doc/reports/manager).

To allow communication between SUnDaE and Manager it is necessary to configure the environment variables in Table 1.

| Variable | Description | Example |
| -- | -- | -- |
| DHIM_HOST| Hostname where Manager is running | localhost |
| DHIM_PORT| Port in the Manager host used to communicate | 5000 |

*Table 1: Environment variables needed to allow communication between SUnDaE and Manager components of Open DISH.*

To use SUnDaE APIs and services you can use the [`docker compose plugin`](https://docs.docker.com/compose/install/) to build the image and startup the container from the `docker-compose.yml` file.

~~~shell
# docker compose command in linux systems
git clone https://gitlab.com/open-dish/sundae.git
cd path/to/sundae/
chmod +x config_env.sh
./config_env.sh
# Install Manager …
export DHIM_HOST=localhost
export DHIM_PORT=5000
docker compose up -d
~~~

After starting the container, to use the SUnDaE API you can access `http://localhost:5483` in your preferred browser. The API documentation in swagger format can be accessed by the endpoint `http://localhost:5483/docs`.

# Data Models and APIs

## Data Models

The models below are based on Synthea models. The ideas presented in openEHR are the best for persistent health data, but it is too complex for this project. Thus, by using the "Synthea models" it’s possible to simplify and at the same time allow the models to expand in the future. The models below aren't a copy of the Synthea documentation, a lot of tables and files were dropped because they wouldn’t be very useful for this project, at least for now.

## Patients

> Version 1.0.2.

Patient demographic data.

### Schema in Relational Format

| Attribute                | Type     | Description           |
|:-------------------------|:---------|:----------------------|
| `id`              | UUID | Primary Key. Unique Identifier of the patient. |
| `birthDate`              | Date (`YYYY-MM-DD`) | The date the patient was born. |
| `deathDate`              | Date (`YYYY-MM-DD`) | The date the patient died. |
| `first`              | String | First name of the patient. |
| `last`              | String | Last or surname of the patient. |
| `marital`              | String | Marital Status. `M` is married, `S` is single. Currently no support for divorce (`D`) or widowing (`W`) |
| `race`              | String | Description of the patient's primary race. |
| `ethnicity`              | String | Description of the patient's primary ethnicity. |
| `gender`              | String | Gender. `M` is male, `F` is female. |
| `birthPlace`              | String | Name of the town where the patient was born. |
| `lat`              | Numeric | Latitude of Patient's address. |
| `lon`              | Numeric | Longitude of Patient's address. |

### Example in JSON Format:

```json
[
  {
  "patient": {
    "id": "1d604da9-9a81-4ba9-80c2-de3375d59b40",
    "birthDate": "1998-05-25",
    "deathDate": "",
    "first": "Mariana",
    "last": "Rutherford",
    "marital": "M",
    "race": "white",
    "ethnicity": "hispanic",
    "gender": "F",
    "birthPlace": "Yarmouth  Massachusetts  US",
    "lat": "42.63614335069588",
    "lon": "-71.3432549217789"
    }
  }
]
```
---

## Observations

> Version 1.0.2.

Patient observations including laboratory tests.

### Schema in Relational Format

| Attribute                | Type     | Description           |
|:-------------------------|:---------|:----------------------|
| `date`              | Date	iso8601 UTC Date (`yyyy-MM-dd'T'HH:mm'Z'`) | The date and time the observation was performed. |
| `patient`              | UUID | Foreign key to the Patient. |
| `encounter`              | UUID | Foreign key to the Encounter where the observation was performed. |
| `category`              | String | Observation category. |
| `code`              | String | Observation or Lab code from LOINC |
| `description`              | String | Description of the observation or lab. |
| `value`              | String | The recorded value of the observation. |
| `units`              | String | The units of measure for the value. |
| `type`              | String | The datatype of `Value`: `text` or `numeric` |

### Example in JSON Format:

```json
[
  {
  "observation": {
    "date": "2012-01-23T17:45:28Z",
    "patient": "1d604da9-9a81-4ba9-80c2-de3375d59b40",
    "encounter": "d0c40d10-8d87-447e-836e-99d26ad52ea5",
    "category": "laboratory",
    "code": "718-7",
    "description": "Hemoglobin [Mass/volume] in Blood",
    "value": "15.1",
    "units": "g/dL",
    "type": "numeric"
    }
  }
]
```

---

## Encounters

> Version 1.1.0.

Patient encounter data.

### Schema in Relational Format

| Attribute                | Type     | Description           |
|:-------------------------|:---------|:----------------------|
| `id`              | UUID | Primary Key. Unique Identifier of the encounter. |
| `start`              | iso8601 UTC Date (`yyyy-MM-dd'T'HH:mm'Z'`) | The date and time the encounter started. |
| `stop`              | iso8601 UTC Date (`yyyy-MM-dd'T'HH:mm'Z'`) | The date and time the encounter concluded. |
| `patient`              | UUID | Foreign key to the Patient. |
| `provider` | UUID | Unique Identifier of the clinicians that provide patient care. |
| `encounterClass`              | String | The class of the encounter, such as `ambulatory`, `emergency`, `inpatient`, `wellness`, or `urgentcare` |
| `code`              | String | Encounter code from SNOMED-CT |
| `description`              | String | Description of the type of encounter. |
| `reasonCode`              | String | Diagnosis code from SNOMED-CT, only if this encounter targeted a specific condition. |
| `reasonDescription`              | String | Description of the reason code. |

### Example in JSON Format:

```json
[
  {
  "encounter": {
    "id": "d0c40d10-8d87-447e-836e-99d26ad52ea5",
    "start": "2010-01-23T17:45:28Z",
    "stop": "2010-01-23T18:10:28Z",
    "patient": "1d604da9-9a81-4ba9-80c2-de3375d59b40",
    "provider": "bde99eb0-3ee8-30b7-8a03-d8b99c91a6b7",
    "encounterClass": "ambulatory",
    "code": "185345009",
    "description": "Encounter for symptom",
    "reasonCode": "10509002",
    "reasonDescription": "Acute bronchitis (disorder)"
    }
  }
]
```

---

## Conditions

> Version 1.0.2.

Patient conditions or diagnoses.

### Schema in Relational Format

| Attribute                | Type     | Description           |
|:-------------------------|:---------|:----------------------|
| `start`              | Date (`YYYY-MM-DD`) | The date the condition was diagnosed. |
| `stop`              | Date (`YYYY-MM-DD`) | The date the condition resolved, if applicable. |
| `patient`              | UUID | Foreign key to the Patient. |
| `encounter`              | UUID | Foreign key to the Encounter when the condition was diagnosed. |
| `code`              | String | Diagnosis code from SNOMED-CT |
| `description`              | String | Description of the condition. |

### Example in JSON Format:

```json
[
  {
  "condition": {
    "start": "2010-03-17",
    "stop": "2010-04-16",
    "patient": "1d604da9-9a81-4ba9-80c2-de3375d59b40",
    "encounter": "d0c40d10-8d87-447e-836e-99d26ad52ea5",
    "code": "36971009",
    "description": "Sinusitis (disorder)"
    }
  }
]
```

---

## Medications

> Version 1.0.2.

Patient medication data.

### Schema in Relational Format

| Attribute                | Type     | Description           |
|:-------------------------|:---------|:----------------------|
| `start`              | iso8601 UTC Date (`yyyy-MM-dd'T'HH:mm'Z'`) | The date and time the medication was prescribed. |
| `stop`              | iso8601 UTC Date (`yyyy-MM-dd'T'HH:mm'Z'`) | The date and time the prescription ended, if applicable. |
| `patient`              | UUID | Foreign key to the Patient. |
| `encounter`              | UUID | Foreign key to the Encounter where the medication was prescribed. |
| `code`              | String | Medication code from RxNorm. |
| `description`              | String | Description of the medication. |
| `dispenses`              | Numeric | The number of times the prescription was filled. |
| `reasonCode`              | String | Diagnosis code from SNOMED-CT specifying why this medication was prescribed. |
| `reasonDescription`              | String | Description of the reason code. |

### Example in JSON Format:

```json
[
  {
  "medication": {
    "start": "2020-11-25T12:12:33Z",
    "stop": "2020-12-30T21:12:49Z",
    "patient": "1d604da9-9a81-4ba9-80c2-de3375d59b40",
    "encounter": "d0c40d10-8d87-447e-836e-99d26ad52ea5",
    "code": "562251",
    "description": "Amoxicillin 250 MG / Clavulanate 125 MG Oral Tablet",
    "dispenses": "1",
    "reasonCode": "36971009",
    "reasonDescription": "Sinusitis (disorder)"
    }
  }
]
```


---

## Allergies

> Version 1.0.2.

Patient allergy data.

### Schema in Relational Format

| Attribute                | Type     | Description           |
|:-------------------------|:---------|:----------------------|
| `start`              | Date (`YYYY-MM-DD`) | The date the allergy was diagnosed. |
| `stop`              | Date (`YYYY-MM-DD`) | The date the allergy ended, if applicable. |
| `patient`              | UUID | Foreign key to the Patient. |
| `encounter`              | UUID | Foreign key to the Encounter when the allergy was diagnosed. |
| `code`              | String | Allergy code |
| `system`              | String | Terminology system of the Allergy code. `RxNorm` if this is a medication allergy, otherwise `SNOMED-CT`. |
| `description`              | String | Description of the `Allergy` |
| `type`              | String | Identify entry as an `allergy` or `intolerance`. |
| `category`              | String | Identify the category as `drug`, `medication`, `food`, or `environment`. |
| `reaction1`              | String | Optional SNOMED code of the patients reaction. |
| `description1`              | String | Optional description of the `Reaction1` SNOMED code. |
| `severity1`              | String | Severity of the reaction: `MILD`, `MODERATE`, or `SEVERE`. |
| `reaction2`              | String | Optional SNOMED code of the patients second reaction. |
| `description2`              | String | Optional description of the `Reaction2` SNOMED code. |
| `severity2`              | String | Severity of the second reaction: `MILD`, `MODERATE`, or `SEVERE`. |

### Example in JSON Format:

```json
[
  {
  "allergy": {
    "start": "2010-03-17",
    "stop": "",
    "patient": "1d604da9-9a81-4ba9-80c2-de3375d59b40",
    "encounter": "d0c40d10-8d87-447e-836e-99d26ad52ea5",
    "code": "762952008",
    "system": "Unknown",
    "description": "Peanut (substance)",
    "type": "allergy",
    "category": "food",
    "reaction1": "247472004",
    "description1": "Wheal (finding)",
    "severity1": "MODERATE",
    "reaction2": "271807003",
    "description2	": "Eruption of skin (disorder)",
    "severity2": "MILD",
    }
  }
]
```

---

## Procedures

> Version 1.0.2.

Patient procedure data including surgeries.

### Schema in Relational Format

| Attribute                | Type     | Description           |
|:-------------------------|:---------|:----------------------|
| `start`              | iso8601 UTC Date (`yyyy-MM-dd'T'HH:mm'Z'`) | The date and time the procedure was performed. |
| `stop`              | iso8601 UTC Date (`yyyy-MM-dd'T'HH:mm'Z'` | The date and time the procedure was completed, if applicable. |
| `patient`              | UUID | Foreign key to the Patient. |
| `encounter`              | UUID | Foreign key to the Encounter where the procedure was performed. |
| `code`              | String | Procedure code from SNOMED-CT |
| `description`              | String | Description of the procedure. |
| `reasonCode`              | String | Diagnosis code from SNOMED-CT specifying why this procedure was performed. |
| `reasonDescription`              | String | Description of the reason code. |

### Example in JSON Format:

```json
[
  {
  "procedure": {
    "start": "2020-11-25T21:12:49Z",
    "stop": "2020-11-25T21:23:25Z",
    "patient": "1d604da9-9a81-4ba9-80c2-de3375d59b40",
    "encounter": "d0c40d10-8d87-447e-836e-99d26ad52ea5",
    "code": "261352009",
    "description": "Face mask (physical object)",
    "reasonCode": "840544004",
    "reasonDescription": "Suspected COVID-19"
    }
  }  
]
```

---

## Image

> Version 2.0.2.

Patient images references.

### Schema in Relational Format

| Attribute                | Type     | Description           |
|:-------------------------|:---------|:----------------------|
| `id`              | UUID | Unique identifier of the image. |
| `date`              | iso8601 UTC Date (`yyyy-MM-dd'T'HH:mm'Z'` | The date and time the image was taken. |
| `patient`              | UUID | Foreign key to the Patient. |
| `encounter`              | UUID | Foreign key to the Encounter where the image was taken. |
| `imagePath`              | String | Path to image in the database. |
| `annotationRaw` | String | Path to the image annotation in ``xml` format, used in model training. |

### Example in JSON Format:

```json
[
  {
  "image": {
    "id": "1d88a815-fbc2-9b61-b582-944100e41fd1",
    "date": "2003-11-01T20:30:34Z",
    "patient": "ada32eac-0c43-38d0-14e5-e2e545631eef",
    "encounter": "13efcb79-c259-e4a5-2258-ea655ceeeb48",
    "imagePath": "images/blood-cell/1d88a815-fbc2-9b61-b582-944100e41fd1.png",
    "annotationRaw": "images/blood-cell/1d88a815-fbc2-9b61-b582-944100e41fd1.xml"
    }
  }  
]
```

At the end of all processing, the models shown will be delivered to the Data Manager via POST in the JSON format specified in the [documentation provided](https://gitlab.com/open-dish/dish/-/blob/development/doc/api/management.md).
Delivery to the Machine Learning Manager is also via POST, with patient and diagnostic report data being contacted and sent in a single request. An example of the JSON sent is shown below.

~~~json
{
        "patient": 
        {
           "id": "1d604da9-9a81-4ba9-80c2-de3375d59b40",
           "birthDate": "1998-05-25",
           "deathDate": "",
           "first": "Mariana",
           "last": "Rutherford",
           "race": "white",
           "ethnicity": "hispanic",
           "gender": "F",
           "birthPlace": "Yarmouth  Massachusetts  US"
           "lat": "42.63614335069588",
           "lon": "-71.3432549217789"
        },
        "diagnostic":
        {
          "id": "42bfe9b8-3f5a-11ed-b878-0242ac120002",
          "patient": "1d604da9-9a81-4ba9-80c2-de3375d59b40",
          "encounter": "d0c40d10-8d87-447e-836e-99d26ad52ea5",
          "status": "final",
          "resultsInterpreter": "cf546bba-6fa5-430f-9656-300dda3cf865",
          "result":[
          {
              "category": "laboratory",
              "code": "718-7",
              "value": "15.1",
              "units": "g/dL",
              "type": "numeric"}
        },
}
~~~

## REST APIs

SUnDaE has two API endpoints that can be used to get patient data. This data can be used as input to other components of Open DISH, to get patient resources or to initiate inference of patient exams using machine learning services.

### Next Patient API

> Version 1.0.0

API used by the client to simulate the patient's entry into the system.

### Get Next Patient

> Version 1.0.0

Returns the next patient's id used to request his data into the bus.

```plaintext
GET /next-patient
```

If successful, returns [`200`](../../api/index.md#status-codes) and the following
response attributes:

| Attribute                | Type     | Description           |
|:-------------------------|:---------|:----------------------|
| `Id`              | UUID | Unique Identifier of the patient. |

* Request:

```shell
curl -X GET "https://gitlab.example.com/api/sundae/next-patient"
```

* Response:

```json
 {
    "id": "1d604da9-9a81-4ba9-80c2-de3375d59b40"
 }
```

### Next Image API

> Version 1.0.0

API used by the client to get data for inference on models that work with images.

### Get Next Image

> Version 1.0.0

Returns image data that can be used to call an inference endpoint of an image model.

```plaintext
GET /next-image
```

If successful, returns [`200`](../../api/index.md#status-codes) and the following
response attributes:

| Attribute | Type | Description |
|:-------|:-------|:-------|
| `id`  | UUID | Unique identifier of the image. |
| `date` | iso8601 UTC Date (`yyyy-MM-dd'T'HH:mm'Z'` | The date and time the image was taken. |
| `patient`   | UUID | Foreign key to the Patient. |
| `encounter`  | UUID | Foreign key to the Encounter where the image was taken. |
| `imagePath` | String | Path to image in the database. |
| `annotationRaw` | String | Path to the image annotation in ``xml` format, used in model training. |

* Request:

```shell
curl -X GET "https://gitlab.example.com/api/sundae/next-image"
```

* Response:

```json
 {
      "id": "1d88a815-fbc2-9b61-b582-944100e41fd1",
      "date": "2003-11-01T20:30:34Z",
      "patient": "ada32eac-0c43-38d0-14e5-e2e545631eef",
      "encounter": "13efcb79-c259-e4a5-2258-ea655ceeeb48",
      "imagePath": "images/blood-cell/1d88a815-fbc2-9b61-b582-944100e41fd1.png",
      "annotationRaw": "images/blood-cell/1d88a815-fbc2-9b61-b582-944100e41fd1.xml"
 }
```

# Usage Examples and Tutorial

As our project is a generation tool for the system, it is not meant to be used in a standalone way, meaning that a tutorial only makes sense for the point of view of the developer, which is covered in the Installation and Execution section.

Outside of this project, our module can be used as a translation tool for Syntheas output to JSON entries. If in the future a user wants to use Synthea data in another `python` project, it is only needed to create a generation module instance and forward its output to the transformation module if a JSON file is desired.


# Highlights

## Raw Data Generator

The first step for generator module functioning is generating raw data, in other words, using Synthea to generate Realistic Synthetic Electronic Health Records (RS-EHRs).

The generator function receives three parameters: the number of patients to generate, if all of them should stay alive, and if all of them should die. By default, Synthea generates patients who remain alive and patients who end up dead too. In the mentioned case, the number of patients given to the generator represents the quantity of alive patients and Synthea generates a random number of dead patients.

Once the data are generated, the CSVs that represent the RS-EHRs are saved in a folder named by the pattern D%Y_%m_%d-T%H_%M_%S, where D represents the date and T the time when the generation occurred, inside the data directory.

~~~python
def generate_patients(
    	num_patients: int, all_alive: bool = False,
    	all_dead: bool = False):
	...
	# Get the actual date and time
	date_time = datetime.now()
	date_time = date_time.strftime('D%Y_%m_%d-T%H_%M_%S')

	# Path to the generated data
	base_directory = f'./data/{date_time}/'

	# Define the shell command to generate the Synthea data
	command = CONST.RUN_SYNTHEA \
          	+ f' -p {num_patients}' \
          	+ ' --generate.append_numbers_to_person_names=false' \
          	+ f' --generate.only_alive_patients={all_alive_str}' \
          	+ f' --generate.only_dead_patients={all_dead_str}' \
          	+ f' --exporter.baseDirectory={base_directory}' \
          	+ ' --exporter.csv.export=true' \
          	+ f' --exporter.csv.excluded_files={excluded_files}' \
          	+ ' --exporter.metadata.export=false' \
          	+ ' --exporter.fhir.export=false' \
          	+ ' --exporter.fhir.transaction_bundle=false' \
          	+ ' --exporter.hospital.fhir.export=false' \
          	+ ' --exporter.practitioner.fhir.export=false'

	# Run Synthea
	subprocess.run(command, shell=True)

	# Complete the path
	base_directory += 'csv/'

	return base_directory
~~~

## Optimization of Transformations

While the data processing is necessary to merge three Synthea databases into only one and make the required changes, this step is necessary to easily convert the data to JSON format subsequently. The first implementation of this transformation was made using multiple `for` structures over the rows of the datasets and building the new database. This approach is bad for performance and gets us a big execution time, so an optimization was made using only `pandas` for the database transformation.

The new version is ten times faster, the Figures 6 and 7 show the difference in a benchmark processing 1000 patients. The [pyinstrument](https://github.com/joerick/pyinstrument) was used to do these benchmarks.

![Benchmarks of the old version of the data transformation](images/benchmark_old_version.jpeg)  
_Figure 6: Benchmark of the old version of the data transformation, approximately 826 seconds to execute._

![Benchmark of the new version of the data transformation](images/benchmark_new_verison.jpeg)  
_Figure 7: Benchmark of the new version of the data transformation, approximately 82 seconds to execute._

## Constant Patient Generator Daily Scheduler
As already mentioned, Constant Patient Generator emulates new patients coming to the hospital. To make it realistic, the `populate()` method is called once a day using FastAPI’s repeated task. The implementation is shown below.

~~~python
...
constant_generation_server = ConstantGenerator(20)
countdown_time = 60 * 60 * 24 # 1 day

# @np_api.on_event("startup")
@repeat_every(seconds=countdown_time)  # 1 day
def daily_generation() -> None:
    constant_generation_server.populate()
~~~

## Redis Usage
[Redis is an open source, in-memory data structure store, used as a database, cache, streaming engine, and message broker.](https://redis.io/) On SUnDaE, Redis is used as an intermediary database, to avoid waiting for the data processing time, patient data is stored on Redis. Its in-memory approach is a great differential and with its use the Next Patient Service increases the performance. Some snippets of the implementation are below.

~~~python
...
patient_stock = redis.Redis(
    host = os.getenv("REDIS_HOST", "open-dish.lab.ic.unicamp.br"),
    port = int(os.getenv("REDIS_PORT", 6379)),
    db = int(os.getenv("REDIS_DB", 0)),
)

def delete_patient_data(patient_id:str) -> None:
    """Deletes the patient data from the database"""
    patient_stock.delete(patient_id)

def get_patient_data(patient_id:str) -> dict:
    """Retrieves the patient data from the database"""
    data = str(patient_stock.get(patient_id))
    ...
    
    return ast.literal_eval(data)

def store_patient_data(patient_id:str, patient_data:dict) -> None:
    """Stores the patient data in the database"""
    patient_stock.set(patient_id, str(patient_data))
~~~

## Database Population

To populate the Open DISH database and therefore provide data to allow the functioning of the machine learning models, we used a bypass given by the MongoDB insert operation, as can be seen in the following code highlight.

We justify this bypass due to the long delay time to complete the POST requests to the database and the need to provide the data as soon as possible. In addition, the database was populated with at least 10,000 patients - the number of alive patients - and the images for one of the machine learning models.

~~~python
...
# Define the client and the database
client = pymongo.MongoClient(mongo_host)
database = client[database]

# Generate the synthetic data
NUM_PATIENTS = 10_000
json_data_generator = JsonDataGenerator(generate_patients(NUM_PATIENTS))
data = json_data_generator.get_json_data()

# Iterate through the collections
for key in data.keys():
	collection = database[key]
	collection.insert_many(data[key])

imagedb_manager = ImageDBManager()
images = imagedb_manager.get_images(imagedb_manager.database_size)
image_collection = database['image']
image_collection.insert_many(images)
~~~

# Improvements and Features
In the future, some improvements are important for the robustness of the system. To mitigate the Next Patient Service long delay time, we started developing a refactored version of the patient stock. This new version stores the patient id as the key and only the data to be sent to the Machine Learning Manager API as its value. All the patient data is sent beforehand, so that we don’t have to wait for all data to be sent before sending the patient id. In order for this to never run out of patients, we tied the patient generator to the Constant Patient Generator. From time to time, it generates a bunch of new patients and already sends all their data to the patient resource manager database. At the same time, it stores the aforementioned data in our Redis database. With all that, we’re always replenishing the patient stock and the required data can be sent way faster to the front end API. Unfortunately this version isn’t quite ready, as we run into some bugs which prevented it from being developed in time, so this could be developed as a future improvement.

Although the infrastructure to allow sending data to machine learning services has been completed, unfortunately, the integration with the Machine Learning Model Manager has not been tested, due to the lack of working models in the testing period. Thus, another necessary future work is to verify the functioning of the communication with the machine learning services and the optimization of this communication as previously mentioned.

Another point of improvement is the application of tests, due to the fast development this item did not receive due attention. Therefore, in the future it is important to develop more tests to ensure application consistency, for new features an approach like TDD, Test Driven Development, might be welcome.


# License

GPL-3
