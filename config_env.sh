#!/bin/bash

# Create open-dish network
docker network rm open-dish | true
docker network create --attachable -d overlay open-dish 

# Create Image Repository if its doesn't exists
if [[ ! -d image_database ]]; then
    export SUNDAE_IMAGEDB_CSV=$PWD/image_database/imagedb.csv
    mkdir -p image_database && cd image_database
    wget https://github.com/MahmudulAlam/Complete-Blood-Cell-Count-Dataset/archive/master.zip
    unzip master.zip && mv Complete-Blood-Cell-Count-Dataset-master blood-cells && cd blood-cells
    rm _config.yml LICENSE README.md && python /home/cl3t0/workspace/open-dish/sundae/sundae/image/generate_imagedb.py
fi

